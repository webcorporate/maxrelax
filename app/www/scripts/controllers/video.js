'use strict';

/**
 * @ngdoc function
 * @name maxRelaxApp.controller:HomecontrollerCtrl
 * @description
 * # HomecontrollerCtrl
 * Controller of the maxRelaxApp
 */
angular.module('MaxRelax')
  .controller('VideoCtrl', function ($scope, $timeout, $rootScope, localStorageService, Dataholdingservice, $state, $ionicViewService, $ionicLoading, $sce) {
    console.log("Video Ctrl ready");
    var controller = this;
    controller.state = null;
    controller.API = null;
    controller.currentVideo = 0;

    controller.onPlayerReady = function(API) {
      controller.API = API;
    };

    controller.config = {
      loop: true,
      preload: "none",
      volume:0,
      sources: $scope.availableTimelapseVideos[0].sources,
      theme: {
        url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
      }
    };

    controller.setVideo = function(index) {
      controller.API.stop();
      controller.API.setVolume(0);
      controller.currentVideo = index;
      controller.config.sources = $scope.availableTimelapseVideos[index].sources;
      $timeout(controller.API.play.bind(controller.API), 100);
    };

    $scope.stopPlaying = function () {
      controller.API.stop();
    };


  });
