'use strict';

/**
 * @ngdoc function
 * @name maxRelaxApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the maxRelaxApp
 */
angular.module('MaxRelax')
  .controller('SettingsCtrl', function ( $rootScope, $scope, $ionicPopover, $ionicPopup, MediaSrv, localStorageService, $ionicLoading, Dataholdingservice, $stateParams, $translate, $timeout, $state, $ionicHistory, APP_CONST, httpSrv, DeviceSrv, SettingsSrv) {

      $scope.triggerResetLocalStorageFromSettings = function() {
          SettingsSrv.resetLocalStorage();
      }


  });
