'use strict';

/**
 * @ngdoc function
 * @name maxRelaxApp.controller:HomecontrollerCtrl
 * @description
 * # HomecontrollerCtrl
 * Controller of the maxRelaxApp
 */
angular.module('MaxRelax')
  .controller('WelcomeCtrl', function ( $rootScope, $scope, $ionicPopover, $ionicPopup, MediaSrv, SettingsSrv, localStorageService, $ionicLoading, Dataholdingservice, $stateParams, $translate, $timeout, $state, $ionicHistory, APP_CONST, httpSrv, DeviceSrv) {

        $scope.downloadData = function() {
            try{
                // Check that any connection to the internet is exist
                if(DeviceSrv.isConnectionExist()) {
                    DeviceSrv.displayToastMessage($translate.instant('PLEASE_DO_NOT_CLOSE_APP_DURING_THE_DOWNLOAD'));
                    setTimeout(function(){
                        downloader.get(APP_CONST.MP3_DOWNLOAD_URL);
                        // INIT PROGRESS VALUES
                        $rootScope.downloadInProgress = true;
                        $rootScope.areFilesDownloaded = false;
                        $rootScope.downloadProgress = 0;
                        $rootScope.downloadProgressButtonText = $translate.instant('DOWNLOADING');
                        console.log("Trying to start download");
                    }, 500);
                } else {
                    DeviceSrv.showAlert($translate.instant('ERROR'), $translate.instant('NO_INTERNET_CONNECTION'), "download");
                }

            } catch(e) {
                console.error(e);
                // JUST FOR TESTING IN THE BROWSER
                DeviceSrv.displayToastMessage($translate.instant('PLEASE_DO_NOT_CLOSE_APP_DURING_THE_DOWNLOAD'));
                console.log("Displaying fake progressbar");
                // INIT PROGRESS VALUES
                $rootScope.downloadInProgress = true;
                $rootScope.areFilesDownloaded = false;
                $rootScope.downloadProgress = null;
                var i = 10;
                var intervalForFakeProgress =  setInterval(function() {
                    console.log("Fake progress is " +i);
                    $rootScope.$apply(function () {
                        i += 1;
                        $rootScope.downloadProgress = i;
                    });

                    if(i == 100) {
                        // SET FAKE ROOTSCOPE VALUES TO ENABLE BUTTONS AND DISPLAY NORMAL MENU
                        $rootScope.$broadcast(APP_CONST.DOWNLOAD_MP3_IS_FINISHED, null);
                        clearInterval(intervalForFakeProgress);
                    }
                }, 100);
            }
        }

    });
