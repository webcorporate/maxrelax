'use strict';

/**
 * @ngdoc function
 * @name maxRelaxApp.controller:HomecontrollerCtrl
 * @description
 * # HomecontrollerCtrl
 * Controller of the maxRelaxApp
 */
angular.module('MaxRelax')
  .controller('SavedCtrl', function ($scope, $timeout, $rootScope, localStorageService, Dataholdingservice, $state, $ionicHistory, $ionicLoading, $translate, $q, APP_CONST) {

    $scope.getPlaylists = function() {
      $timeout(function () {
        $scope.$apply(function () {
          $rootScope.playlists = localStorageService.get(APP_CONST.LS_PLAYLIST_DATA);
          console.log($scope.playlists);
          console.log("Playlists loaded");
        });
      }, 150);
    };

    $scope.playSelectedItem = function(index) {
      // Get data
      var dataToPass = $scope.playlists[index];
      $rootScope.$broadcast('setPlayListEvent', dataToPass);
      // Prepare interestitial
      if($rootScope.isItFullVersion == false) {
        // Make it async
        var deferred = $q.defer();
        $rootScope.AdMob.prepareInterstitial({
          adId: $rootScope.admobid.interstitial,
          autoShow: false
        });

        deferred.resolve();
        return deferred.promise;
      }
    };


    $scope.RemoveAllPlaylists = function() {
      try {
        console.log($scope.playlists.length);
        if($scope.playlists.length == 0) {
          $scope.showAlert("Add some playlists", "Nothing to remove");
        } else {
          var playlists = [];
          $scope.playlists = playlists;
          localStorageService.set("playlists", playlists);
          $ionicLoading.show({
            duration: 1000,
            template: $translate.instant('CLEARED')
          });
        }
      } catch(e) {
        $scope.showAlert($translate.instant('ERROR'), $translate.instant('CANNOT_REMOVE_SOUND_FROM_SELCTION'));
      }
    };

    $scope.removePlaylist = function(index) {
      try {
        console.log("Trying to remove playlist on index " +index);

        var playlists = $scope.playlists;
        playlists.splice(index,1);
        localStorageService.set("playlists", playlists);
        $ionicLoading.show({
          duration: 1000,
          template: $translate.instant('REMOVED')
        });
      } catch(e) {
        $scope.showAlert($translate.instant('ERROR'), $translate.instant('CANNOT_REMOVE_SOUND_FROM_SELCTION'));
      }
    };


  });
