'use strict';

/**
 * @ngdoc function
 * @name maxRelaxApp.controller:HomecontrollerCtrl
 * @description
 * # HomecontrollerCtrl
 * Controller of the maxRelaxApp
 */
angular.module('MaxRelax')
  .controller('HomeCtrl', function ( $rootScope, $scope, $ionicPopover, $ionicPopup, MediaSrv, localStorageService, $ionicLoading, Dataholdingservice, $stateParams, $translate, $timeout, $state, $ionicHistory, APP_CONST, httpSrv, DeviceSrv) {
        var i = 0;
        $scope.isPlayingPaused = false;
        $scope.playlistName = '';
        $scope.playlist = {};
        //$rootScope.selectedSounds = [];

        // Receive broadcast
        $rootScope.$on('setPlayListEvent', function(event, playListData) {
            // DISPLAY Interstitial if playlist is selected
            if($rootScope.isItFullVersion == false && $rootScope.AdMob != undefined) {
                $rootScope.AdMob.showInterstitial();
                $rootScope.AdMob.createBanner({
                  adId : $rootScope.admobid.banner,
                  position : $rootScope.AdMob.AD_POSITION.BOTTOM_CENTER,
                  adSize: 'SMART_BANNER',
                  isTesting: false,
                  autoShow : true
                });
            }
            $scope.stopAllSelectedSounds();
            $scope.setPlaylistToView(playListData);
        });

        $scope.setPlaylistToView = function(playListData) {
          try{
            console.log('State params are:');
            console.log(typeof playListData);
            console.log(playListData);
            if(typeof(playListData)  !== 'undefined') {
              // REDIRECT
              $state.go('app.home', null);
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              console.log("Trying to set playlist data");
              // SET PLAYLIST SOUNDS TO SCOPE
              $rootScope.selectedSounds = playListData.tracks;
              $scope.playlistName = playListData.name;
              // SHOW MESSAGE
              $ionicLoading.show({
                duration: 1000,
                template: $translate.instant('PLAYLIST_LOADED')
              });
            }
          } catch(e) {
              DeviceSrv.showAlert($translate.instant('ERROR'), $translate.instant('CANNOT_SET_PLAYLIST_DATA'));
          }

        };

        // Display Popover
        $scope.openPopover = function($event, templateName) {
            // Init popover on load
            $ionicPopover.fromTemplateUrl('templates/'+templateName, {
                scope: $scope,
            }).then(function(popover) {
                $scope.popover = popover;
                $scope.popover.show($event);
            });
        };

        $scope.closePopover = function() {
            $scope.popover.hide();
        };

        $scope.removeActualSavedPlaylist = function() {
          // Reset possible selected playlist in play view;
          $rootScope.SelectedPlaylistData = "";
          $rootScope.selectedSounds = [];
        };

        $scope.addSoundToSelection = function(index, type) {
            try {
                var myArray = $rootScope.selectedSounds;
                var selectedItem;
                if(type == 'singles' )
                      selectedItem = $scope.availableSounds[index];
                if(type == 'compositions' )
                      selectedItem = $scope.availablePrepreparedSounds[index];
                // Add status signalizing state of the track (playing, stopped)
                selectedItem.state = 0;

                console.log("Selected item is folowing: ");
                console.log(selectedItem);
                console.log(myArray);
                console.log(myArray.indexOf(selectedItem));
                if (myArray.indexOf(selectedItem) === -1) {
                    $rootScope.selectedSounds.push(selectedItem);

                    DeviceSrv.displayToastMessage(selectedItem.name+" "+$translate.instant('ADDED'), APP_CONST.TOAST_TIMEOUT_VERY_SHORT);
                    return;
                }
                if (myArray.indexOf(selectedItem) != -1) {
                    DeviceSrv.displayToastMessage($translate.instant('CANNOT_ADD_SAME_ITEM_TWICE'), APP_CONST.TOAST_TIMEOUT_LONG);
                }

            } catch(e) {
                DeviceSrv.showAlert($translate.instant('ERROR'), $translate.instant('CANNOT_ADD_SOUND_TO_SELECTION'));
            }
        };

        $scope.removeSoundFromSelection = function(index) {
            try {
                if($rootScope.selectedSounds[index]){
                    console.log("TRYING TO REMOVE AND STOP ITEM ON THE INDEX "+index+" with name" +$rootScope.selectedSounds[index].name);

                    $timeout(function() {
                        $rootScope.$apply(function () {
                            //$rootScope.selectedSounds[index] = null;
                            $rootScope.selectedSounds.splice(index, 1);
                        });
                    });

                    if($rootScope.selectedSounds[index].state == 1) {
                        $scope.stopSelectedItem(index);
                    }

                    DeviceSrv.displayToastMessage($translate.instant('REMOVED'), APP_CONST.TOAST_TIMEOUT_VERY_SHORT);
                }
            } catch(e) {
                DeviceSrv.showAlert($translate.instant('ERROR'), $translate.instant('CANNOT_REMOVE_SOUND_FROM_SELCTION'));
            }
        };

        $scope.setVolumeLevelForItem = function(index, volume) {
            try {
                console.log(volume);
                $rootScope.selectedSounds[index].defaultVolume = volume;
                var media = $rootScope.selectedSounds[index].mediaInstance;
                media.setVolume(volume);
                console.log(media);
            } catch(e) {
                console.error("Cannot set volume level");
                //DeviceSrv.showAlert("Error", "Cannot set volume level");
            }
        };

        $scope.setPlaylistName = function( playlistName) {
            try {
                console.log(playlistName);
                $scope.playlistName = playlistName;
            } catch(e) {
                DeviceSrv.displayToastMessage($translate.instant('CANNOT_SET_PLAYLIST_NAME'), APP_CONST.TOAST_TIMEOUT_LONG);
            }
        };

        /*
         * EVENTS LISTENERS
         */
        $scope.$on(APP_CONST.EVT_RESET_LOCALSTORAGE, function() {
            $scope.resetLocalStorage(true);
        });

        $scope.$on(APP_CONST.EVT_START_DOWNLOAD, function() {
            //$scope.downloadData();
        });

        $scope.$on(APP_CONST.EVT_DOWNLOAD_PROGRESS_CHANGED, function(event, progress) {
            setTimeout(function(){
                $rootScope.$apply(function () {
                $rootScope.downloadProgressButtonText = $translate.instant('DOWNLOADING') + progress+ "%";
                $rootScope.downloadProgress =  progress;
                console.log("Actualized progress to" +progress);
              });
            },300);
        });

        $scope.$on(APP_CONST.DOWNLOAD_MP3_IS_FINISHED, function(event) {
            console.log(APP_CONST.DOWNLOAD_MP3_IS_FINISHED);
            // UDPATE SOUND VERSION AFTER UDPATE
            if(Dataholdingservice.getAvailableSoundVersion() !== null
              || typeof Dataholdingservice.getAvailableSoundVersion() != 'undefined') {
                console.info("Updating new version number");
                localStorageService.set(APP_CONST.LS_MP3_VERSION, Dataholdingservice.getAvailableSoundVersion());
            }
            localStorageService.set(APP_CONST.LS_MP3_ALREADY_DOWNLOADED, true);
            DeviceSrv.hideToastMessage();
            $rootScope.$apply(function () {
                $rootScope.downloadInProgress = false;
                $rootScope.areFilesDownloaded = true;
                $rootScope.welcomePopUpDisplayed = false;
                DeviceSrv.displayToastMessage($translate.instant('SOUNDS_SUCCESSFULLY_DOWNLOADED_ENJOY_THE_APP'), APP_CONST.TOAST_TIMEOUT_LONG);
                $state.go('app.home', null);
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
            });
        });

        $scope.$on(APP_CONST.EVT_DOWNLOAD_ERROR, function(error) {
            console.log(APP_CONST.EVT_DOWNLOAD_ERROR);
            console.log(error);
            $rootScope.$apply(function () {
                DeviceSrv.showAlert($translate.instant('DOWNLOAD_ERROR'), $translate.instant('FILES_CANNOT_BE_DOWNLOADED'), "reset");
            });
        });

        $scope.$on(APP_CONST.EVT_UNDEFINED_DOWNLOAD_ERROR, function(error) {
            console.log(APP_CONST.EVT_UNDEFINED_DOWNLOAD_ERROR);
            $rootScope.$apply(function () {
                DeviceSrv.showAlert($translate.instant('DOWNLOAD_ERROR'), $translate.instant('FILES_CANNOT_BE_DOWNLOADED_UNDEFINED_ERROR'), "reset");
            });
        });

        $scope.$on(APP_CONST.EVT_RESET_DOWNLOAD_PROCESS, function(error) {
            console.log(APP_CONST.EVT_RESET_DOWNLOAD_PROCESS);
            $scope.resetLocalStorage(true);
        });

        $rootScope.$on(APP_CONST.EVT_CONNECTION_CHANGE, function(event, networkState) {
            console.log("Connenction changed");
            console.log(APP_CONST.EVT_CONNECTION_CHANGE);
            console.log(networkState);
        });

        $scope.playSelectedItem = function(index) {
            try {
                if($scope.isPlayingPaused == true) {
                    console.log("Trying to pause audio");
                    console.log($rootScope.selectedSounds.length);
                    for(var i=0;i <= $rootScope.selectedSounds.length -1 ;i++){
                      console.log("Resuming");
                      $rootScope.selectedSounds[i].mediaInstance.play();
                    }
                    $scope.someSoundsArePlaying = true;
                    $scope.isPlayingPaused = false;
                } else {
                  var fileName = $rootScope.selectedSounds[index].file;
                  var volume = $rootScope.selectedSounds[index].defaultVolume;
                  if($rootScope.selectedSounds[index].type == "composition")
                    var filePath  = APP_CONST.PATH_TO_COMPOSITIONS+fileName+".mp3";
                  if($rootScope.selectedSounds[index].type == "single")
                    var filePath  = APP_CONST.PATH_TO_SINGLES +fileName+".mp3";
                  console.log(filePath);
                  MediaSrv.loadMedia(
                      filePath,
                      function onError(err){ console.log('onError', MediaSrv.getErrorMessage(err)); },
                      function onStatus(status){ console.log('onStatus', MediaSrv.getStatusMessage(status)); },
                      function onStop(){
                          console.log('onStop');
                          console.log('Index is following');
                          console.log(index);
                          // Hack for continuous playing after the end of the track file
                          var stateToCheck = $rootScope.selectedSounds[index] || 'undefined';
                          console.log(typeof stateToCheck);
                          console.log(stateToCheck);
                          if(stateToCheck == 'undefined') {
                              console.log('Decrement index');
                              index --;
                              console.log('Decremented index');
                              console.log(index);
                          }
                          if($rootScope.selectedSounds[index].state == 1) {
                            console.log('For sound with name:'+$rootScope.selectedSounds[index].name+' is index ' +index+' is state '+$rootScope.selectedSounds[index].state);
                            $rootScope.$apply(function () {
                              $rootScope.selectedSounds[index].mediaInstance.play();
                            });
                          }

                      }
                  ).then(function(media){
                      console.log("Then media");
                      media.play({ numberOfLoops: 999 });
                      media.setVolume(volume);
                      $rootScope.selectedSounds[index].state = 1;
                      $rootScope.selectedSounds[index].mediaInstance = media;
                      $scope.someSoundsArePlaying = true;
                  });
                }
            } catch(e) {
                alert(JSON.stringify(e));
                console.log(e);
                DeviceSrv.showAlert($translate.instant('ERROR'), $translate.instant('ERROR_DURING_THE_PLAYING_ITEM'));
            }
        };

        var mediaSuccess = function (status) {
            alert("mediaSuccess");
            alert(JSON.stringify(status));
            /*
             if (status === Media.MEDIA_STOPPED) {
             myMedia.play();
             }
             */
        };

        $scope.playAllSelectedSounds = function() {
            try {
                console.log($rootScope.selectedSounds.length);
                if($rootScope.selectedSounds.length == 0) {
                    DeviceSrv.displayToastMessage($translate.instant('ADD_SOME_SOUNDS_FIRST_PLEASE'), APP_CONST.TOAST_TIMEOUT_SHORT);
                } else {
                    angular.forEach($rootScope.selectedSounds, function loadMedia(selectedSound, idx){
                        $scope.playSelectedItem(idx);
                    });
                    $scope.someSoundsArePlaying = true;
                }
            } catch(e) {
                alert(JSON.stringify(e));
                console.log(e);
                DeviceSrv.showAlert("Error", "Error during the playing item");
            }
        };

        $scope.pauseAllSelectedSounds = function() {
          try {
            console.log($rootScope.selectedSounds.length);
            if($rootScope.selectedSounds.length == 0) {
                DeviceSrv.displayToastMessage($translate.instant('ADD_SOME_SOUNDS_FIRST_PLEASE'), APP_CONST.TOAST_TIMEOUT_SHORT);
            } else {
              angular.forEach($rootScope.selectedSounds, function loadMedia(selectedSound, idx){
                if($rootScope.selectedSounds[idx].state == 1) {
                  $scope.pauseSelectedItem(idx);
                }
              });
              $scope.isPlayingPaused = true;
              $scope.someSoundsArePlaying = false;
            }
          } catch(e) {
            alert(JSON.stringify(e));
            console.log(e);
            DeviceSrv.showAlert("Error", "Error during the playing item");
          }
        };

        $scope.stopAllSelectedSounds = function() {
            try {
                console.log("Stopping all selected sounds");
                console.log($rootScope.selectedSounds.length);

                angular.forEach($rootScope.selectedSounds, function loadMedia(selectedSound, idx){
                    if($rootScope.selectedSounds[idx].state == 1) {
                        $scope.stopSelectedItem(idx);
                    }
                });

                $timeout(function() {
                    $rootScope.$apply(function () {
                        $rootScope.someSoundsArePlaying = false;
                    });
                });

            } catch(e) {
                alert(JSON.stringify(e));
                console.log(e);
                DeviceSrv.showAlert("Error", "Error during the playing item");
            }
        };

        $scope.initWatchCollection = function() {
            $scope.$watchCollection('selectedSounds', function () {
                console.log("Collection change");
                var leng = 0;
                angular.forEach($rootScope.selectedSounds, function loadMedia(selectedSound, idx) {
                    if ($rootScope.selectedSounds[idx].state == 1) {
                        leng++;
                    }
                });

                if (leng <= 0) {
                    $scope.someSoundsArePlaying = false;
                    console.log("No sounds are playing");
                }
                if (leng > 0) {
                    $scope.someSoundsArePlaying = true;
                    console.log("Some sounds are playing");
                }
            });
        }

        $scope.stopSelectedItem = function(index) {
            try {
                console.log("trying to stop selected item "+ $rootScope.selectedSounds[index].name);
                console.log("on index "+index);
                var leng = 0;
                if($rootScope.selectedSounds[index].state == 1) {
                    var mediaInstance = $rootScope.selectedSounds[index].mediaInstance;
                    console.log("Media instance IS");
                    console.log(mediaInstance);
                    $rootScope.selectedSounds[index].state = 0;
                    mediaInstance.stop();
                    $rootScope.selectedSounds[index].mediaInstance = "";
                }

              angular.forEach($rootScope.selectedSounds, function loadMedia(selectedSound, idx){
                    if($rootScope.selectedSounds[idx].state == 1) {
                        leng ++;
                    }
                });

                if(leng <= 0) {
                    $scope.someSoundsArePlaying = false;
                    console.log("No sound are playing");
                }
                if(leng > 0) {
                    $scope.someSoundsArePlaying = true;
                    console.log("Some sound are playing");
                }
                console.log("Leng is:");
                console.log(leng);
            } catch(e) {
                alert(JSON.stringify(e));
                console.log(e);
                DeviceSrv.showAlert("Error", "Cannot stop playing of item");
            }
        };

        $scope.pauseSelectedItem = function(index) {
          try {
            var leng = 0;
            if($rootScope.selectedSounds[index].state == 1) {
              var mediaInstance = $rootScope.selectedSounds[index].mediaInstance;
              mediaInstance.pause();
            }

          } catch(e) {
            alert(JSON.stringify(e));
            console.log(e);
            DeviceSrv.showAlert("Error", "Cannot stop playing of item");
          }
        };

        $scope.setTimer = function(timer) {
            console.log(timer);
            $scope.timer = timer;
        };

        $scope.saveTimer = function(timer) {
            if($rootScope.selectedSounds.length == 0) {
                DeviceSrv.displayToastMessage("Cannot use timer for empty list", APP_CONST.TOAST_TIMEOUT_LONG);
            } else {
                $scope.clearCountDownIntervals();
                var animationTimerId = setInterval(function () {
                    $("#minutesLeft").fadeTo(100, 0.1).fadeTo(200, 1.0);
                }, 1000);
                Dataholdingservice.setAnimationId(animationTimerId);
                Dataholdingservice.setMinutesLeft(timer);
                $scope.closePopover();
                $scope.countDown();

            }
        };

        $scope.clearCountDownIntervals = function() {
            $("#minutesLeft").clearQueue().finish();
            // Clear previously set animations
            console.log(Dataholdingservice.getAnimationId());
            if (Dataholdingservice.getAnimationId() != null) {
              console.log("Interval for animation cleared");
              clearInterval(Dataholdingservice.getAnimationId());
            }
            if (Dataholdingservice.getTimerId() != null) {
                console.log("Interval for counter is cleared");
                clearInterval(Dataholdingservice.getTimerId());
            }
        };

        $scope.countDown = function() {
                $rootScope.viewModel = {};
                var minutesLeft = Dataholdingservice.getMinutesLeft();
                $rootScope.viewModel.minutesLeft = minutesLeft;
                $rootScope.isCounterDisplayed = Dataholdingservice.isCounterDisplayed();
                var timerId = setInterval(function() {
                console.log("Counting down");
                minutesLeft -- ;
                console.log("Decreasing minutes");
                console.log(minutesLeft);
                Dataholdingservice.setMinutesLeft(minutesLeft);
                console.log("minutes left " + Dataholdingservice.getMinutesLeft());
                $rootScope.$apply(function () {
                  $rootScope.viewModel.minutesLeft = Dataholdingservice.getMinutesLeft();
                  console.log("Actualized time applied");
                });

                  if(minutesLeft <= 0) {
                      console.log("Time left");
                      $scope.clearCountDownIntervals();
                      console.log(Dataholdingservice.isCounterDisplayed());
                      $scope.hideCounter();
                      $scope.stopAllSelectedSounds();

                  }

                }, 1000 * 60);

                Dataholdingservice.setTimerId(timerId);
        };

        $scope.hideCounter  = function() {
            console.log("Hidding the counter");
            $rootScope.$apply(function () {
              $rootScope.isCounterDisplayed = false;
            });
        };

        $scope.cancelTimer = function() {
            clearInterval(Dataholdingservice.getTimerId());
            clearInterval(Dataholdingservice.getAnimationId());
            $("#minutesLeft").hide();
            DeviceSrv.displayToastMessage($translate.instant('TIMER_CANCELED'), APP_CONST.TOAST_TIMEOUT_SHORT);
        };

        $scope.shareIt = function() {
          try {
            console.log("Trying to share");
            DeviceSrv.displayToastMessage($translate.instant('WAIT_PLEASE'), APP_CONST.TOAST_TIMEOUT_LONG);
            var urlToShare;
            var platform = device.platform;
            switch(platform) {
              case "Android":
                urlToShare = "https://play.google.com/store/apps/details?id=com.Max.Relax";
                break;
              case "iOS":
                urlToShare = "https://itunes.apple.com/us/app/max-relax/id910136532?mt=8&uo=4";
                break;
              default:
                urlToShare = "https://play.google.com/store/apps/details?id=com.kedoo.thedeadbeginning";
            }
            var message = {
              text: $translate.instant('SHARE_PREDEFINED_MESSAGE'),
              url: urlToShare
            };
            window.socialmessage.send(message);
          } catch(e) {
              alert(e.message);
          }
        };

        $scope.savePlaylist = function() {
            try {
                var leng = $rootScope.selectedSounds.length;
                console.log(leng);
                console.log($scope.playlistName);
                var existingPlaylists = localStorageService.get(APP_CONST.LS_PLAYLIST_DATA);

                if(leng <= 0) {
                    DeviceSrv.displayToastMessage($translate.instant('CANNOT_SAVE_EMPTY_PLAYLIST'), APP_CONST.TOAST_TIMEOUT_LONG);
                    return true;
                }
                if($scope.playlistName == "") {
                    DeviceSrv.displayToastMessage($translate.instant('ENTER_PLAYLIST_NAME_PLEASE'), APP_CONST.TOAST_TIMEOUT_LONG);
                    return true;
                }
                else {
                    console.log("Trying to save playlist");
                    console.log($rootScope.selectedSounds);
                    // HACK FOR REMOVING OBJECT REFERENCE
                    var playlistInstance = JSON.parse(JSON.stringify($rootScope.selectedSounds));
                    var preparedSoundsForSave = [];
                    angular.forEach(playlistInstance, function (selectedSound, key){
                      console.log("Sound to playlist is following:");
                      console.log(selectedSound);
                      var selectedSoundInstance = selectedSound;
                      delete selectedSoundInstance.mediaInstance;
                      delete selectedSoundInstance.$$hashKey;
                      selectedSoundInstance.state = 0;
                      preparedSoundsForSave.push(selectedSoundInstance);
                    });


                    console.info("Existing playlist are following");
                    console.info(existingPlaylists);

                    console.info("Sound which will be saved");
                    console.info(preparedSoundsForSave);

                    // Create the new object which has been saved
                    $scope.playlist.name = $scope.playlistName;
                    $scope.playlist.tracks = preparedSoundsForSave;
                    console.log($scope.playlist);
                    existingPlaylists.push($scope.playlist);
                    localStorageService.set(APP_CONST.LS_PLAYLIST_DATA, existingPlaylists);
                    $ionicLoading.show({
                        duration: 1000,
                        template: $translate.instant('SAVED')
                    });
                    $scope.playlistName = "";
                    $scope.closePopover();
                }
            } catch(e) {
                alert(JSON.stringify(e));
                console.log(e);
                DeviceSrv.showAlert("Error", "Exception during saving the playlist");
            }
        };
        
        $scope.clearPlaylistFromPlaying = function() {
            console.log('Trying to clear playlist');
            $scope.stopAllSelectedSounds();
            $rootScope.selectedSounds = [];
            $scope.playlistName = "";
            $scope.removeActualSavedPlaylist();
        };

        $scope.triggerPopover = function() {
            var availableSoundsBtn = document.getElementById('availableSoundsBtn');
            $timeout(function() {
                angular.element(availableSoundsBtn).triggerHandler('click');
            }, 100);
        };

        $scope.openCreatorPage = function() {
            console.log("Opening phonetoaster");
            window.open("http://www.phonetoaster.com/?source=Mobile_MaxRelax", "_system");
        };

        $scope.openExternalMarket = function() {
          console.log("Triggering popover");
          var platform = device.platform;
          switch(platform.toLowerCase()) {
            case "android":
              window.open("market://search?q=pub:"+APP_CONST.PUBLISHER_ID_GOOGLE_PLAY, "_system");
              break;
            case "ios":
              window.open("https://itunes.apple.com/us/artist/"+APP_CONST.PUBLISHER_ID_APPSTORE, "_system");
              break;
          }
        };

        $scope.buyFullVersion = function() {
          console.log("Triggering but full version");
          var platform = device.platform;
          switch(platform.toLowerCase()) {
            case "android":
              window.open("market://details?id=com.Max.Relax.full", "_system");
              break;
            case "ios":
              window.open("https://itunes.apple.com/us/app/age-my-face-pro-free-photo/id422704707?mt=8&uo=4", "_system");
              break;
          }
        };


        $scope.readSoundsFile = function() {
            DeviceSrv.displayToastMessage($translate.instant('LOADING'), APP_CONST.TOAST_TIMEOUT_SHORT);
            DeviceSrv.readFile($rootScope.storagePath+"/"+APP_CONST.MP3_FOLDER_NAME+"/sounds/",APP_CONST.MP3_FILE_NAME,
                APP_CONST.IS_IT_HIGH_PRIORITY_FILE)
                .then(function(value){
                    console.log(value);
                    var parsedObject = JSON.parse(value);
                    console.log(parsedObject);
                    $scope.availableSounds = parsedObject.availableSounds;
                    $scope.availablePrepreparedSounds = parsedObject.availablePrepreparedSounds;
            }, function(error){
                    $scope.closePopover();
                    console.error("Read sound file failed");
                    console.error(error);
                    DeviceSrv.showAlert($translate.instant('ERROR'), $translate.instant('SOUND_FILE_IS_MISSING'), 'reset');
                });
        }


  });
