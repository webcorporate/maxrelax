'use strict';
// Ionic Starter App, v0.9.20

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('MaxRelax', ['ionic',
  'config',
  'MaxRelax',
  'LocalStorageModule',
  'pascalprecht.translate',
  "ngSanitize",
  "angular-svg-round-progress",
  "ngCordova"
])
//  DEFINE CONSTANTS FOR WHOLE APP
.constant("APP_CONST", {
  "LS_MP3_ALREADY_DOWNLOADED": "mp3_already_downloaded",
  "LS_PLAYLIST_DATA": "playlist_data",
  "LS_MP3_VERSION": "version",
  "MP3_DOWNLOAD_URL": "http://phonetoaster.com/scripts/maxrelax/sounds.zip",
  "MP3_ACTUAL_VERSION_URL": "http://phonetoaster.com/scripts/maxrelax/version.php",
  "EVT_DOWNLOAD_ERROR": "evt_download_error",
  "EVT_UNDEFINED_DOWNLOAD_ERROR": "evt_undefined_download_error",
  "EVT_CONNECTION_CHANGE": "evt_connection_change",
  "EVT_START_DOWNLOAD": "evt_start_download",
  "EVT_DOWNLOAD_PROGRESS_CHANGED": "downloadProgressEvt",
  "EVT_RESET_LOCALSTORAGE": "evt_reset_localstorage",
  "EVT_RESET_DOWNLOAD_PROCESS": "evt_reset_download_process",
  "PATH_TO_COMPOSITIONS": "sounds/compositions/",
  "PATH_TO_SINGLES": "sounds/singles/",
  "MP3_FOLDER_NAME": "maxrelax",
  "MP3_FILE_NAME": "sounds.json",
  "DOWNLOAD_MP3_IS_FINISHED": "download_mp3_is_finished",
  "TOAST_TIMEOUT_VERY_SHORT": 500,
  "TOAST_TIMEOUT_SHORT": 1000,
  "TOAST_TIMEOUT_LONG": 1500,
  "DEBUG_MODE_ENABLED": 0,
  "IS_IT_HIGH_PRIORITY_FILE": true,
  "IOS_BANNER": "ca-app-pub-6024201770538830/1387792705",
  "IOS_INTERESTRIAL": "ca-app-pub-6024201770538830/4341259107",
  "ANDROID_BANNER": "ca-app-pub-6024201770538830/6795197907",
  "ANDROID_INTERESTRIAL": "ca-app-pub-6024201770538830/8271931106",
  "WP_BANNER": "ca-app-pub-6869992474017983/8878394753",
  "WP_INTERESTRIAL": "ca-app-pub-6869992474017983/1355127956",
  "PUBLISHER_ID_GOOGLE_PLAY": "PhoneToaster",
  "PUBLISHER_ID_APPSTORE": "webcorporate/id535562579",

})
.run(function($ionicPlatform, $rootScope, $translate, localStorageService, APP_CONST, $q, DeviceSrv, SettingsSrv) {
  $ionicPlatform.ready(function() {

    // INIT EVERYTHING WHAT IS NEED
    SettingsSrv.setPlatform();
    SettingsSrv.setFullScreen();
    DeviceSrv.initDownloader();

    // IF MP3 are not downloaded, show welcome page + init dowloader
    if(!SettingsSrv.areMp3AlreadyDowloaded()) {
      SettingsSrv.redirectToWelcome();
      SettingsSrv.setDefaultPlaylist();
    }

    DeviceSrv.handleBackButton();
    DeviceSrv.setOnlineOfllineListeners();


    // Set  background mode for iOS
    var isIOS = ionic.Platform.isIOS();
    if(isIOS === true) {
      SettingsSrv.enableBackgroundPlayingForIos();
      $rootScope.isiOS = true;
    }

    $rootScope.$apply(function () {
      // Global settings
      $rootScope.debugMode = APP_CONST.DEBUG_MODE_ENABLED;
      $rootScope.isItFullVersion = false;
      $rootScope.deviceIsOnline = null;
      $rootScope.selectedSounds = [];
    });

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    if(window.cordova) {
      var admobid = {};
      if( /(android)/i.test(navigator.userAgent) ) {
        admobid = { // for Android
          banner: APP_CONST.ANDROID_BANNER,
          interstitial: APP_CONST.ANDROID_INTERESTRIAL
        };
      } else if(/(ipod|iphone|ipad|ios)/i.test(navigator.userAgent)) {
        admobid = { // for iOS
          banner: APP_CONST.IOS_BANNER,
          interstitial: APP_CONST.IOS_INTERESTRIAL
        };
      } else {
        admobid = { // for Windows Phone
          banner: APP_CONST.WP_BANNER,
          interstitial: APP_CONST.WP_INTERESTRIAL
        };
      }
      if (AdMob && $rootScope.isItFullVersion == false) {
        // Make it async
        var deferred = $q.defer();

        AdMob.createBanner({
          adId : admobid.banner,
          position : AdMob.AD_POSITION.BOTTOM_CENTER,
          adSize: 'SMART_BANNER',
          isTesting: false,
          autoShow : true
        });

        AdMob.prepareInterstitial({
          adId : admobid.interstitial,
          autoShow : false
        });
        $rootScope.AdMob = AdMob;
        $rootScope.admobid = admobid;

        deferred.resolve();
        return deferred.promise;
      }
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, localStorageServiceProvider) {
    //SET LS PREFIX
   localStorageServiceProvider.setPrefix('max_relax');

   $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'HomeCtrl'
    })
     .state('app.welcome', {
       cache: false,
       url: '/welcome',
       views: {
         'menuContent' :{
           templateUrl: 'templates/welcome.html',
           controller: 'WelcomeCtrl'
         }
       }
     })
     .state('app.update', {
       cache: false,
       url: '/update',
       views: {
         'menuContent' :{
           templateUrl: 'templates/update.html',
           controller: 'HomeCtrl'
         }
       }
     })
    .state('app.home', {
      cache: false,
      url: '/home:playlistData',
      views: {
        'menuContent' :{
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
    })
    .state('app.saved', {
      cache: false,
      url: '/saved',
      views: {
        'menuContent' :{
          templateUrl: 'templates/saved.html',
          controller: 'SavedCtrl'
        }
      }
    })
     .state('app.settings', {
       cache: false,
       url: '/settings',
       views: {
         'menuContent' :{
           templateUrl: 'templates/settings.html',
           controller: 'SettingsCtrl'
         }
       }
     })
    .state('app.about', {
      cache: false,
      url: '/about',
      views: {
        'menuContent' :{
          templateUrl: 'templates/about_app.html',
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});

