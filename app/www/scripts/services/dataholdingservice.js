'use strict';

/**
 * @ngdoc service
 * @name maxRelaxApp.dataHoldingService
 * @description
 * # dataHoldingService
 * Service in the maxRelaxApp.
 */
angular.module('MaxRelax')
  .service('Dataholdingservice', function Dataholdingservice() {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var media = [];
        var minutesLeft = 0;
        var animationId;
        var timerId;
        var availableSoundVersion;

        var addMedia = function(newObj) {
            media.push(newObj);
        };

        var getMediaInstances = function(){
            return media;
        };

        var getAnimationId = function() {
          return animationId;
        };

        var setAnimationId = function(animationIdPar) {
          animationId = animationIdPar;
        };

        var setTimerId = function(timerIdPar) {
          timerId = timerIdPar;
        };

        var getTimerId = function() {
          return timerId;
        };

        var getMinutesLeft = function(){
          return minutesLeft;
        };

        var setMinutesLeft = function(minutes){
          minutesLeft = minutes;
        };

        var isCounterDisplayed = function(){
          if(minutesLeft > 0) {
            return true;
          } else {
            return false
          }
        };

        var getAvailableSoundVersion = function(){
            return availableSoundVersion;
        };

        var setAvailableSoundVersion = function(version){
            availableSoundVersion = version;
        };

    return {
            addMedia: addMedia,
            getMediaInstances: getMediaInstances,
            setTimerId: setTimerId,
            getTimerId: getTimerId,
            getMinutesLeft: getMinutesLeft,
            setMinutesLeft: setMinutesLeft,
            getAnimationId: getAnimationId,
            setAnimationId: setAnimationId,
            isCounterDisplayed: isCounterDisplayed,
            getAvailableSoundVersion: getAvailableSoundVersion,
            setAvailableSoundVersion: setAvailableSoundVersion,
        };
  });
