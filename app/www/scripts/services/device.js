'use strict';

/**
 * @ngdoc service
 * @name maxRelaxApp.MediaSrv
 * @description
 * # MediaSrv
 * Factory in the maxRelaxApp.
 */
angular.module('MaxRelax')
    .factory('DeviceSrv', function ($q, $ionicPlatform, $ionicPopup, $window, $rootScope, $translate, APP_CONST, localStorageService, $ionicLoading, $cordovaFile) {

        var isDownloadProcessAlreadyFired = false;



        var isConnectionExist = function () {
            console.info("Checking that internet connection is available");
            try {
                var networkState = window.navigator.onLine;
                return networkState;
            } catch(e) {
                networkState = undefined;
                return networkState;
            }
        };

        var handleBackButton = function() {
            document.addEventListener("backbutton", function(e){
                if($rootScope.welcomePopUpDisplayed == true) {
                    console.log("Trying to sdsds");
                    showAlert($translate.instant('WELCOME'), $translate.instant('NO_FILES'), "download");
                    e.preventDefault();
                } else {
                   return;
                }
            }, false);
        }

        var showAlert = function(title, message, flag) {
            hideToastMessage();
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: message
            });

            alertPopup.then(function(res) {
                console.log('Alert popup button tapped');
                if(flag == 'reset') {
                    console.log('Resetting download process');
                    $rootScope.$broadcast(APP_CONST.EVT_RESET_DOWNLOAD_PROCESS, null);
                }
            });
        };

        var initDownloader = function () {
            try {

                if (typeof downloader !== "undefined") {
                    // Init downloader
                    downloader.init({folder: APP_CONST.MP3_FOLDER_NAME, unzip: true, fileSystem: cordova.file.externalRootDirectory });
                    console.log("Downloader from app is initialized");
                    // Set event listeners for downloader
                    document.addEventListener("DOWNLOADER_downloadSuccess", function (event) {
                        var data = event.data;
                        console.log(data);
                        console.info("Mp3 Sounds downloaded");
                        $rootScope.$broadcast(APP_CONST.DOWNLOAD_MP3_IS_FINISHED, null);
                        $rootScope.downloadInProgress = false;
                    });
                    document.addEventListener("DOWNLOADER_downloadError", function (error) {
                        $rootScope.$broadcast(APP_CONST.EVT_DOWNLOAD_ERROR, error);
                        console.error(error);
                    });
                    document.addEventListener("DOWNLOADER_error", function (error) {
                        $rootScope.$broadcast(APP_CONST.EVT_UNDEFINED_DOWNLOAD_ERROR, error);
                        console.error(error);
                    });
                    document.addEventListener("DOWNLOADER_gotFileSystem", function (event) {
                        var data = event.data;
                        console.log("FILE SYSTEM PATH");
                        console.debug(data);
                        $rootScope.storagePath = data[0].nativeURL;
                    });
                    document.addEventListener("DOWNLOADER_downloadProgress", function (event) {
                        if(isDownloadProcessAlreadyFired == false) {
                            console.info("Download event process event fired");
                            isDownloadProcessAlreadyFired = true;
                        }
                        var eventToBroadcast = 'downloadProgressEvt';
                        var data = event.data;
                        //console.log(data);
                        $rootScope.$broadcast(eventToBroadcast, data[0]);
                    });
                } else {
                    console.error("Downloader is not available");
                }
            } catch (e) {
                console.error(e);
            }
        }

        var setOnlineOfllineListeners = function () {
            // Set event listeners for online/offline states
            document.addEventListener("online", function () {
                $rootScope.$apply(function () {
                    $rootScope.$broadcast(APP_CONST.EVT_CONNECTION_CHANGE, {networkIsAvailable: true});
                });
            }, false);
            document.addEventListener("offline", function () {
                $rootScope.$apply(function () {
                    $rootScope.$broadcast(APP_CONST.EVT_CONNECTION_CHANGE, {networkIsAvailable: false});
                });
            }, false);
        }

        var disableBackButton = function () {
            /*
            $ionicPlatform.offHardwareBackButton(function () {
                console.log("Hardware button is disabled");
            })
            */
            /*
            $ionicPlatform.registerBackButtonAction(function (event) {
                alert("Disabled");
                event.preventDefault();
            }, 100);
            */

            /*
            $ionicPlatform.registerBackButtonAction(function (event) {
                event.preventDefault();
                event.stopPropagation();
            }, 100);
            */
        }

        var enableBackButton = function () {
            /*
            console.log("Trying to enable backbutton");
            $ionicPlatform.onHardwareBackButton(function () {
                console.log("Hardware button is enabled");
            })
            */
        }

        var displayToastMessage = function (message, time) {
            if (time == null || typeof time == 'undefined') {
                $ionicLoading.show({
                    template: message
                });
            } else {
                $ionicLoading.show({
                    template: message,
                    duration: time,
                });
            }
        }

        var hideToastMessage = function () {
            $ionicLoading.hide();
        }


        var deleteFolder = function (path,dir) {
            console.info("Trying delete folder with URI: " +path+ " "+dir);
            $cordovaFile.removeDir(path, dir)
                .then(function (success) {
                    // success
                    console.log("Success");
                }, function (error) {
                    console.error(error);
                });

        }


        var getUserAgent = function() {
            console.log("Trying to get user agent");
            return navigator.userAgent;
        }


        var readFile = function (path, file, isItPriorityFile) {
            console.info("Trying to read file with URI: "+path+""+file);
            var deferred = $q.defer();
            $cordovaFile.checkFile(path, file).then(function (success) {
                console.log(success);
                $cordovaFile.readAsText(path, file)
                    .then(function (success) {
                        // success
                        console.log("Success");
                        console.log(success);
                        deferred.resolve(success);
                    }, function (error) {
                        console.error(error);
                        deferred.reject(error);
                        displayToastMessage($translate.instant('FILE_CANNOT_READ'), APP_CONST.TOAST_TIMEOUT_SHORT);
                    });
            }, function (error) {
                console.error(error);
                displayToastMessage($translate.instant('FILE_NOT_EXIST'), APP_CONST.TOAST_TIMEOUT_SHORT);
                deferred.reject(error);
            });
            return deferred.promise;
        }

        var showPromptDialog = function ( message, title) {
            console.info("Trying to show prompt dialog");
            var deferred = $q.defer();
                var confirmPopup = $ionicPopup.confirm({
                    title: title,
                    template: message
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        console.log('You are sure');
                    } else {
                        console.log('You are not sure');
                    }
                    deferred.resolve(res);
                });
            return deferred.promise;
        }

        // Init functions
        return {
            isConnectionExist: isConnectionExist,
            initDownloader: initDownloader,
            setOnlineOfllineListeners: setOnlineOfllineListeners,
            displayToastMessage: displayToastMessage,
            hideToastMessage: hideToastMessage,
            disableBackButton: disableBackButton,
            enableBackButton: enableBackButton,
            handleBackButton: handleBackButton,
            showAlert: showAlert,
            deleteFolder: deleteFolder,
            getUserAgent: getUserAgent,
            readFile: readFile,
            showPromptDialog: showPromptDialog,
        };

    });
