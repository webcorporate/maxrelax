'use strict';

/**
 * @ngdoc service
 * @name maxRelaxApp.SettingsSrv
 * @description
 * # SettingsSrv
 * Factory in the maxRelaxApp.
 */
angular.module('MaxRelax')
    .factory('SettingsSrv', function ($q, $ionicPlatform, $ionicPopup, $window, $ionicHistory, $rootScope, $translate, APP_CONST, localStorageService, $ionicLoading, $cordovaFile, $state, DeviceSrv, httpSrv) {


        var areMp3AlreadyDowloaded = function () {
            console.info("Checking that MP3 are downloaded");
            if(localStorageService.get(APP_CONST.LS_MP3_ALREADY_DOWNLOADED) === null ||
                localStorageService.get(APP_CONST.LS_MP3_ALREADY_DOWNLOADED) == 'false') {
                console.info("MP3 are not downloaded");
                return false;
            } else {
                console.info("Redirecting to home");
                $rootScope.areFilesDownloaded = true;
                this.checkAppSoundsVersion();
                return true;
            }
        }

        var redirectToWelcome = function () {
            $rootScope.areFilesDownloaded = false;
            $rootScope.downloadProgressButtonText = $translate.instant('DOWNLOAD_SOUNDS');
            console.info("Redirectiong to welcome");
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('app.welcome', null);

        }

        var checkAppSoundsVersion = function () {
            console.info("Trying to check new version of sounds and actual version of MP3");
            if(localStorageService.get(APP_CONST.LS_MP3_VERSION) == undefined) {
                httpSrv.getData("GET",APP_CONST.MP3_ACTUAL_VERSION_URL).then(function(data) {
                    console.log(data);
                    localStorageService.set(APP_CONST.LS_MP3_VERSION, data.actual_version);
                });
            } else {
                // CHECK FOR NEW VERSION
                console.log("Sounds version already saved");
                if(DeviceSrv.isConnectionExist()) {
                    httpSrv.getData("GET",APP_CONST.MP3_ACTUAL_VERSION_URL).then(function(data) {
                        console.log(data);
                        // DISPLAY INFO ABOUT NEW VERSION
                        if(data.actual_version > localStorageService.get(APP_CONST.LS_MP3_VERSION)) {
                            Dataholdingservice.setAvailableSoundVersion(data.actual_version);
                            console.info("New version is available");
                            // DISPLAY PROMPT FROM SERVICE
                            DeviceSrv.showPromptDialog("Do You want to download?","New version is available").then(function(data) {
                                console.log(data);
                                // PROCESS DOWNLOAD OF THE NEW VERSION
                                if(data == true) {
                                    console.info("Trying to get new version " +Dataholdingservice.getAvailableSoundVersion());
                                    $state.go('app.update', null);
                                    $ionicHistory.nextViewOptions({
                                        disableBack: true
                                    });
                                }
                            });
                        } else {
                            console.info("Actual version of the sounds is latest");
                        }
                    });
                }
            }
        }

        // Disable fullscreen and enable toolbar
        var setFulScreen = function () {
            ionic.Platform.fullScreen(false,true);
        }

        var resetLocalStorage = function () {
            console.info("Resetting local storage");
            localStorageService.set(APP_CONST.LS_MP3_ALREADY_DOWNLOADED, false);
            localStorageService.set(APP_CONST.LS_PLAYLIST_DATA, []);
            $rootScope.areFilesDownloaded = false;
            $rootScope.downloadInProgress = false;
            DeviceSrv.displayToastMessage($translate.instant('LOCALSTORAGE_RESETED'), APP_CONST.TOAST_TIMEOUT_SHORT);
            //DeviceSrv.deleteFolder($rootScope.storagePath, APP_CONST.MP3_FOLDER_NAME);
            //DeviceSrv.deleteFolder(cordova.file.externalRootDirectory, APP_CONST.MP3_FOLDER_NAME);
            this.redirectToWelcome();
        }

        var enableBackgroundPlayingForIos = function () {
            // cordova.plugins.backgroundMode is now available
            if (typeof cordova != 'undefined') {
                cordova.plugins.backgroundMode.enable();
                // Run in background without notification
                cordova.plugins.backgroundMode.configure({
                    silent: true
                });
                // The title, ticker and text for that notification can be customized as follows:
                cordova.plugins.backgroundMode.setDefaults({
                    title:  $translate.instant('MAX_RELAX_IS_PLAYING_ON_BACKGROUND'),
                    ticker: $translate.instant('PLAYING_ON_BACKGROUND_ACTIVATED'),
                    text:   $translate.instant('TAP_TO_RETURN_INTO_THE_APP')
                });
            }
        }

        var setDefaultPlaylist = function () {
            console.log("Trying to set default user setttings");
            if(localStorageService.get(APP_CONST.LS_PLAYLIST_DATA) == undefined) {
                console.log("First run, localstorage not initialized");
                localStorageService.set(APP_CONST.LS_PLAYLIST_DATA, []);
            } else {
                console.log("User setttings already saved");
            }
        }

        var setPlatform = function () {
            console.log("Setting platform");
            var isWebView = ionic.Platform.isWebView();
            var isIOS = ionic.Platform.isIOS();
            var isAndroid = ionic.Platform.isAndroid();

            if(isIOS === true) {
                $rootScope.platform = 'ios';
                $rootScope.platformClass = 'searchSoundFieldIos';
            }
            if(isAndroid === true) {
                $rootScope.platform = 'android';
                $rootScope.platformClass = 'searchSoundFieldAndroid';
            }
            if(isIOS === false && isAndroid === false) {
                $rootScope.platform = 'android';
                $rootScope.platformClass = 'searchSoundFieldAndroid';
            }
            console.log($rootScope.platform);
        }


        // Init functions
        return {
            areMp3AlreadyDowloaded: areMp3AlreadyDowloaded,
            redirectToWelcome: redirectToWelcome,
            setFullScreen: setFulScreen,
            enableBackgroundPlayingForIos: enableBackgroundPlayingForIos,
            setDefaultPlaylist: setDefaultPlaylist,
            setPlatform: setPlatform,
            checkAppSoundsVersion: checkAppSoundsVersion,
            resetLocalStorage: resetLocalStorage,
        };



    });
