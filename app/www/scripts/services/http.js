'use strict';

angular.module('MaxRelax')
    .factory('httpSrv', function httpSrv($http, $q) {

        return {
            getData: getData
        };

        function getData(method, url) {
            var def = $q.defer();
            $http({method:method, url:url})
                .success(function (data, status, headers, config) {
                    console.log("Success");
                    console.log(status);
                    def.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    console.log("Error");
                    def.reject("Failed to get");

                });

            return def.promise;
        }

        // I WANT HANDLE ERROR STATES HERE AND RETURN FORMATTED MESSAGE WITH ERROR
        // DESCRIPTION IN CASE OF THE ERROR STATE (service unavailable, 404, etc..)
        function getErrorMessage(status) {
            console.log(status);
        }
    });
